import json
import os
from zipfile import ZipFile


def extract_info(zip_path):
    zipfile = ZipFile(zip_path)
    target_file="content.json"
    try:
        with zipfile.open(target_file) as file_in_zip:
            # Read the content.json
            content = json.load(file_in_zip)
    except KeyError:
        # If the content.json is not found, try opening with "./" prefix
        alt_target_file = f"./{target_file}"
        try:
            with zipfile.open(alt_target_file) as file_in_zip:
                content = json.load(file_in_zip)
        except KeyError:
            print(f"File {target_file} not found in the zip archive.")
 
    Title=f"{content['title']}"
    Image=f"{content['image']}"
    Summary=f"{content['summary']}"
    target_image=f"{content['image']}".replace('\\','/')
    UrlZip='https://drane-grenoble.forge.apps.education.fr/banque_epoc'+str(zip_path).replace('/builds/drane-grenoble/banque_epoc','')

    global HTML
    global Compteur
    Compteur+=1
    HTML+='<div class="tuile" id="'+str(Compteur)+'"><div class="face" style="background-image:url(\'https://drane-grenoble.forge.apps.education.fr/banque_epoc/'+target_image+'\');"><div class="titre filtre">'+Title+'</div><div class="liens"><a href="#" class="btn btn-secondary" onclick="Verso(this,'+str(Compteur)+');"> 🔎 </a>&nbsp;&nbsp;<a class="btn btn-primary" href="'+UrlZip+'"/> 🔗 </a>&nbsp;&nbsp;<a class="btn btn-info" href="#" onclick="myQR(\'google.fr\','+str(Compteur)+');"><img class="icone" src="qr-code.png"></a></div></div><div class="face verso"><div class="resume filtre">'+Summary+'</div><div class="qrcode"></div><script>new QRCode(document.getElementById('+str(Compteur)+').getElementsByClassName(\'qrcode\')[0],"'+UrlZip+'");</script><div class="retour"><a href="#" class="btn btn-secondary" onclick="Recto(this,'+str(Compteur)+');">⏏️</a></div></div></div>\n'

    
    try:
        zipfile.extract(target_image)
    except KeyError:
         # If the content.json is not found, try opening with "./" prefix
        alt_target_image = f"./{content['image']}".replace('\\','/')
        try:
            zipfile.extract(alt_target_image)
        except KeyError:
            print(f"File {target_image} not found in the zip archive.")

def iterate_over_zips(directory):
    for root, dirs, files in os.walk(directory):
        for file in files:
            if file.lower().endswith('.epoc'):
                zip_path = os.path.join(root, file)
                print(f"Extracting {zip_path}\n")
                extract_info(zip_path)


if __name__ == "__main__":
    HTML='<html><head><meta charset="UTF-8"><meta name="viewport" content="width=device-width, initial-scale=1.0"> <link rel="stylesheet" href="style.css"><title>Banque d\'ePOC</title><script src="scripts.js"></script><script src="qrcode.min.js"></script></head><body><img src="https://forge.apps.education.fr/drane-grenoble/banque_epoc/badges/main/pipeline.svg" style="position:absolute;bottom:0px;right:0px;"/><div><h1> Banque d\'ePOC</h1><input class="form-control" id="searchbar" onkeyup="search_epoc()" type="text" name="search" placeholder="Chercher un ePoc..."></div>\n'
    Compteur=0
    target_directory = os.getcwd()
    iterate_over_zips(target_directory)
    with open('index.html','w',) as index:
        index.write(HTML+'<br/></body></html>')
